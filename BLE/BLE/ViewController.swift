//
//  ViewController.swift
//  BLE
//
//  Created by pbanavara on 13/07/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet var titleLabel: UIView!
    
    var centralManager:CBCentralManager!
    var peripheral:CBPeripheral!
    
    let pmServiceUUID = CBUUID(string: "713D0000-503E-4C75-BA94-3148F18D941E")
    let pmServiceTxUUID = CBUUID(string: "713D0002-503E-4C75-BA94-3148F18D941E")
    let pmServiceRxUUID = CBUUID(string: "713D0003-503E-4C75-BA94-3148F18D941E")
    //let RBL_SERVICE_UUID = "713D0000-503E-4C75-BA94-3148F18D941E"
    

    override func viewDidLoad() {
        super.viewDidLoad()
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func centralManagerDidUpdateState(central: CBCentralManager!) {
        if central.state == CBCentralManagerState.PoweredOn {
            NSTimer.scheduledTimerWithTimeInterval(100, target: self, selector: Selector("scanTimeout"), userInfo: nil, repeats: false)
            central.scanForPeripheralsWithServices([pmServiceUUID], options: nil)
            self.statusLabel.text = "Scanning for devices"
        } else {
            println("Bluetooth is not powered on")
        }
    }
    
    @objc private func scanTimeout() {
        
        println("[DEBUG] Scanning stopped")
        self.centralManager.stopScan()
    }
    
    func centralManager(central: CBCentralManager!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [NSObject : AnyObject]!, RSSI: NSNumber!) {
        
        let nameOfDeviceFound = (advertisementData as NSDictionary).objectForKey(CBAdvertisementDataLocalNameKey) as? NSString
        //if (nameOfDeviceFound == deviceName) {
            self.statusLabel.text = "PM2.5 sensor found"
            
            self.peripheral = peripheral
            self.peripheral.delegate = self
            self.centralManager.connectPeripheral(peripheral, options: nil)
//        //} else {
//            println("PM2.5 sensor not found")
//        }
    }
    
    func centralManager(central: CBCentralManager!, didConnectPeripheral peripheral: CBPeripheral!) {
        self.statusLabel.text = "Discovering peripheral services"
        peripheral.discoverServices(nil)
    }
    
    func peripheral(peripheral: CBPeripheral!, didDiscoverServices error: NSError!) {
        self.statusLabel.text = "Looking at Peripheral services"
        for service in peripheral.services {
            let thisService = service as! CBService
            peripheral.discoverCharacteristics(nil, forService: thisService)
            
            println(service.UUIDString)
        }

    }
    
    func peripheral(peripheral: CBPeripheral!, didDiscoverCharacteristicsForService service: CBService!, error: NSError!) {
        self.statusLabel.text = "Enabling sensors"
        var enableByte = 1
        let enableBytes = NSData(bytes: &enableByte, length: sizeof(UInt8))
        for ch in service.characteristics {
            let thisCh = ch as! CBCharacteristic
            println(thisCh.UUID.description)
            if thisCh.UUID == pmServiceTxUUID {
                self.peripheral.setNotifyValue(true, forCharacteristic: thisCh)
                self.peripheral.readValueForCharacteristic(thisCh)                
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral!, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!) {
        self.statusLabel.text = "connected"
        let dataBytes = characteristic.value
        /*
        let dataLength = dataBytes.length
        var dataArray = [Int8](count: dataLength, repeatedValue: 0)
        dataBytes.getBytes(&dataArray, length: dataLength * sizeof(Int8))
        println(dataArray)
        */
        var sensorValue: NSInteger = 0
        characteristic.value.getBytes(&sensorValue, length: 4)
        println(sensorValue)
        self.statusLabel.text = String(sensorValue)
        
        // Element 1 of the array will be ambient temperature raw value
        // let ambientTemperature = Double(dataArray[1])/128
        
        // Display on the Status label
        //var dataString = NSString(data: dataBytes, encoding:NSUTF8StringEncoding)
        //self.statusLabel.text = String(dataString!)
    }
    
    


}

