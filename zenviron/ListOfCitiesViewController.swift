//
//  ListOfCitiesViewController.swift
//  zenviron
//
//  Created by pbanavara on 03/06/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import UIKit

class ListOfCitiesViewController: UITableViewController {
    
    var searchCityModels:SearchCityModels!
    var selectedCityModel:CityModel!
    var selectedCityIndex:Int!
    var bgView:UIImageView!
    @IBOutlet weak var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if searchCityModels != nil {
            return searchCityModels.cityModels.count
        } else {
            return 0
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cityCell", forIndexPath: indexPath) as! CityCellViewTableViewCell
        let row = indexPath.row
        var pmHourly = searchCityModels.cityModels[row].pmHourlyData
        if !pmHourly.isEmpty {
            cell.insidePmLabel.text = searchCityModels.cityModels[row].pmHourlyData[0]["AQIValue"] as? String
            cell.insidePmLabel.textColor = Utility.buildAQIBackgroundColor(searchCityModels.cityModels[row].aqiCategory)
            cell.insideCityLabel.text = searchCityModels.cityModels[row].name
            cell.insideTimeLabel.text = searchCityModels.cityModels[row].localTime
            

        }
        cell.insideCityLabel.sizeToFit()
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "backToCitySegue" {
            var multiCityController = segue.destinationViewController as! CityViewController
            multiCityController.selectedIndex = selectedCityIndex
            multiCityController.cities = searchCityModels
        } else if segue.identifier == "searchCitySegue" {
            var searchCityController = segue.destinationViewController as! SearchViewController
            searchCityController.listOfCityModels = searchCityModels
        }
    }
    
    @IBAction func addNewRow(sender: UIButton) {
        self.performSegueWithIdentifier("searchCitySegue", sender: self)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let indexPath = tableView.indexPathForSelectedRow();
        selectedCityIndex = indexPath?.item
        let currentCell = tableView.cellForRowAtIndexPath(indexPath!)
        self.performSegueWithIdentifier("backToCitySegue", sender: self)
        Flurry.logEvent("In ListOfCities clicked on row \(indexPath?.row)")
        
    }
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row != 0 {
            if editingStyle == .Delete {
                // Delete the row from the data source
                searchCityModels.cityModels.removeAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            } else if editingStyle == .Insert {
                // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
            }
        }
        
    }
    
    
}
