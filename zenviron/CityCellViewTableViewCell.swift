//
//  CityCellViewTableViewCell.swift
//  zenviron
//
//  Created by pbanavara on 15/06/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import UIKit
import CoreLocation

class CityCellViewTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    @IBOutlet weak var insideTimeLabel: UILabel!
    @IBOutlet weak var insideCityLabel: UILabel!
    @IBOutlet weak var insidePmLabel: UILabel!
    
    var loc:CLLocationCoordinate2D!
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
