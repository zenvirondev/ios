//
//  InCityView.swift
//  zenviron
//
//  Created by pbanavara on 26/06/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import Foundation
import UIKit

class InCityView: UIView, UIScrollViewDelegate {
    
    @IBOutlet weak var cLabel: UILabel!
    @IBOutlet weak var pmLabel: UILabel!
    @IBOutlet weak var verticalScrollView: UIScrollView!
    @IBOutlet weak var ozoneLabel: UILabel!
    @IBOutlet weak var ozoneValue: UILabel!
    @IBOutlet weak var pmValue: UILabel!
    @IBOutlet weak var hourlyScrollData: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var prevDailyPeak: UICollectionView!
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var inHourlyScrollView: UIView!
    @IBOutlet weak var inScrollOzone: UILabel!
    @IBOutlet weak var inScrollTime: UILabel!
    @IBOutlet weak var inScrollPm: UILabel!
    
    @IBOutlet weak var aqiCategory: UILabel!
    
    @IBOutlet weak var inScrollOzone1: UILabel!
    @IBOutlet weak var inScrollPM1: UILabel!
    @IBOutlet weak var inScrollTime1: UILabel!
    
    @IBOutlet weak var inScrollOzone2: UILabel!
    @IBOutlet weak var inScrollPM2: UILabel!
    @IBOutlet weak var inScrollTime2: UILabel!
    
    @IBOutlet weak var inScrollOzone3: UILabel!
    @IBOutlet weak var inScrollPM3: UILabel!
    @IBOutlet weak var inScrollTime3: UILabel!
    
    @IBOutlet weak var inScrollOzone4: UILabel!
    @IBOutlet weak var inScrollPM4: UILabel!
    @IBOutlet weak var inScrollTime4: UILabel!

    @IBOutlet weak var inScrollOzone5: UILabel!
    @IBOutlet weak var inScrollPM5: UILabel!
    @IBOutlet weak var inScrollTime5: UILabel!
    
    @IBOutlet weak var inScrollOzone6: UILabel!
    @IBOutlet weak var inScrollPM6: UILabel!
    @IBOutlet weak var inScrollTime6: UILabel!
   
    
    @IBOutlet weak var inDailyScrollView: UIScrollView!
    
    @IBOutlet weak var inScrollDPPM: UILabel!
    @IBOutlet weak var inScrollDPOzone: UILabel!
    @IBOutlet weak var inScrollDay: UILabel!
    
    @IBOutlet weak var inScrollDPPM1: UILabel!
    @IBOutlet weak var inScrollDPOzone1: UILabel!
    @IBOutlet weak var inScrollDay1: UILabel!
    
    @IBOutlet weak var inScrollDPPM2: UILabel!
    @IBOutlet weak var inScrollDPOzone2: UILabel!
    @IBOutlet weak var inScrollDay2: UILabel!

    @IBOutlet weak var inScrollDPPM3: UILabel!
    @IBOutlet weak var inScrollDPOzone3: UILabel!
    @IBOutlet weak var inScrollDay3: UILabel!

    @IBOutlet weak var inScrollDPPM4: UILabel!
    @IBOutlet weak var inScrollDPOzone4: UILabel!
    @IBOutlet weak var inScrollDay4: UILabel!
    
    @IBOutlet weak var inScrollDPPM5: UILabel!
    @IBOutlet weak var inScrollDPOzone5: UILabel!
    @IBOutlet weak var inScrollDay5: UILabel!

    @IBOutlet weak var inScrollDPPM6: UILabel!
    @IBOutlet weak var inScrollDPOzone6: UILabel!
    @IBOutlet weak var inScrollDay6: UILabel!

    @IBOutlet weak var aqiDescriptionLabel: UILabel!
}
