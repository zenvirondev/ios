//
//  InsideView.swift
//  znew
//
//  Created by pbanavara on 15/07/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import UIKit

class InsideView: UIView {

    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var verticalScroll: UIScrollView!
                
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var sevenDayTableView: UITableView!
    @IBOutlet weak var aqiCategory: UILabel!
    @IBOutlet weak var pmValue: UILabel!
    
    @IBOutlet weak var aqiDescriptionLabel: UILabel!
   
    @IBOutlet weak var outsideView: UIView!
    @IBOutlet weak var hourlyScrollView: UIScrollView!
}
extension UIView {
    class func loadFromNibNamed(nibNamed: String, bundle : NSBundle? = nil) -> InsideView? {
        
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiateWithOwner(nil, options: nil)[0] as? InsideView
    }
}
