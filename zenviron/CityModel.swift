//
//  CityModel.swift
//  zenviron
//
//  Created by pbanavara on 04/06/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import Foundation
import CoreLocation

class CityModel {
    
    var name: String!
    var aqiCategory: String!
    
    var coordinates:CLLocationCoordinate2D!
    var pmHourlyData:Array<NSDictionary>!
    var pmForecastData:Array<NSDictionary>!
    var pmDailyPeak:Array<NSDictionary>!
    
    var ozoneHourlyData:Array<NSDictionary>!
    var ozoneForecastData:Array<NSDictionary>!
    var ozoneDailyPeak:Array<NSDictionary>!
    var localTime: String!
    
    
    init(name:String) {
        self.name = name
        self.aqiCategory = ""
        
        pmHourlyData = []
        pmDailyPeak = []
        pmForecastData = []
        
        ozoneDailyPeak = []
        ozoneHourlyData = []
        ozoneForecastData = []
        coordinates = CLLocationCoordinate2D()
        localTime = ""
        
        
    }
    
    
}
