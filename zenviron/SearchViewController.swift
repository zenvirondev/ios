//
//  SearchViewController.swift
//  zenviron
//
//  Created by pbanavara on 03/06/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var itemsWithNoBusiness:NSMutableArray!
    var cityModel:CityModel!
    var listOfCityModels:SearchCityModels!
    var selectedIndex:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.becomeFirstResponder()
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchByGooglePlaces(inputString: String) {
        var placesClient = GMSPlacesClient()
        var placesFilter = GMSAutocompleteFilter()
        placesFilter.type = GMSPlacesAutocompleteTypeFilter.City
        
        if (count(inputString) > 0) {
            placesClient.autocompleteQuery(inputString, bounds: nil, filter: nil, callback: {(results, error) -> Void in
                if error != nil {
                    println("Autocompletion error ::: \(error)")
                } else {
                    self.itemsWithNoBusiness = NSMutableArray(array: results!)
                    self.tableView.reloadData()
                }
            })
        }
        
       
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        searchByGooglePlaces(searchText)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if itemsWithNoBusiness == nil {
            return 0
        } else {
            return itemsWithNoBusiness.count
            
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let indexPath = tableView.indexPathForSelectedRow();
        let currentCell = tableView.cellForRowAtIndexPath(indexPath!)
        var curObj = itemsWithNoBusiness[indexPath!.item] as! GMSAutocompletePrediction
        var cityName = curObj.attributedFullText.string
        var newCity = cityName.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        var cn = newCity.stringByAddingPercentEscapesUsingEncoding(NSUnicodeStringEncoding)
        var url = Constants.baseUrl + "?type=all&address=\(cn!)"
        selectedIndex = listOfCityModels.cityModels.count - 1
        fetchData(url, cityName:curObj.attributedFullText.string )
        Flurry.logEvent("Clicked on cityName :: \(cityName)")
        
    }
    
    func fetchData(url: String, cityName: String) {
        let urlPath = NSURL(string: url)
        let request = NSURLRequest(URL: urlPath!)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) { (response, data, error) -> Void in
            var jsonData = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as? NSDictionary
            if jsonData == nil {
                jsonData = NSDictionary()
            }
            var model = DataFetch().processJson(jsonData!)
            var ns = split(cityName) {$0 == ","}
            if (ns.count == 1) {
                model.name = cityName
            } else {
                model.name = ns[0] + "," + ns[1]
            }
            self.listOfCityModels.cityModels.append(model)
            dispatch_async(dispatch_get_main_queue(), {
                self.performSegueWithIdentifier("cityViewSegue", sender: SearchViewController.self)
            })
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->  UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! UITableViewCell
        var displayObject = (itemsWithNoBusiness[indexPath.row] as! GMSAutocompletePrediction)
        cell.textLabel!.attributedText = displayObject.attributedFullText
        cell.textLabel?.textColor = UIColor.whiteColor()
        
        //cell.loc = (itemsWithNoBusiness[indexPath.row] as! MKMapItem).placemark.coordinate
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "cityViewSegue" {
            var cityView = segue.destinationViewController as! CityViewController
            cityView.cities = listOfCityModels
            cityView.selectedIndex = selectedIndex + 1
        }
        if segue.identifier == "backToListSegue" {
            var listOfCities = segue.destinationViewController as! ListOfCitiesViewController
            listOfCities.searchCityModels = listOfCityModels

        }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.performSegueWithIdentifier("backToListSegue", sender: SearchViewController.self)
    }
    
    
}
