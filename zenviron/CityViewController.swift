//
//  CityViewController.swift
//  zenviron
//
//  Created by pbanavara on 02/06/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import UIKit
import MapKit
import CoreBluetooth

class CityViewController: UIViewController, UIScrollViewDelegate, CLLocationManagerDelegate, CBCentralManagerDelegate, CBPeripheralDelegate, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var cityScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var cityName: String!
    var selectedIndex:Int = Int(0)
    var cities:SearchCityModels!
    var buildViewPoint:CGFloat!
    var locationManager:CLLocationManager!
    var locationFlag:Bool = true
    var wakeupFlag = false
    
    var childViews = Array<InsideView>()
    
    var centralManager:CBCentralManager!
    var peripheral:CBPeripheral!
    
    let pmServiceUUID = CBUUID(string: "713D0000-503E-4C75-BA94-3148F18D941E")
    let pmServiceTxUUID = CBUUID(string: "713D0002-503E-4C75-BA94-3148F18D941E")
    let pmServiceRxUUID = CBUUID(string: "713D0003-503E-4C75-BA94-3148F18D941E")
    //let RBL_SERVICE_UUID = "713D0000-503E-4C75-BA94-3148F18D941E"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        centralManager = CBCentralManager(delegate: self, queue: nil)
        buildViewPoint = CGFloat(0.0)
        
        cityScrollView.setTranslatesAutoresizingMaskIntoConstraints(false)
        cityScrollView.delegate = self
        
        cities = SearchCityModels.sharedInstance
        
        updateModels()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "appDidBecomeActive:",
            name: UIApplicationWillEnterForegroundNotification , object: nil)
    }
    
    func appDidBecomeActive(notification: NSNotification) {
        locationFlag = true
        updateModels()
    }
    
    func updateModels () {
        for var i = cities.cityModels.count-1; i>=0; i-- {
            if i == 0 {
                // First city hence get the new location
                startLocating()
            } else {
                var coordinate = cities.cityModels[i].coordinates
                var address = (cities.cityModels[i].name).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
                var lat = coordinate.latitude.description
                var lng = coordinate.longitude.description
                var url = Constants.baseUrl + "?type=all&address=\(address!)"
                fetchData(i, url: url)
            }
            
        }
        
    }
    
    func startLocating() {
        if CLLocationManager.locationServicesEnabled() {
            if locationManager == nil {
                locationManager = CLLocationManager()
                locationManager.requestWhenInUseAuthorization()
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
                locationManager.distanceFilter = 20
                locationManager.startMonitoringSignificantLocationChanges()
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    //Called when the location changes. Need to make this code better
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        var loc = locations.last as! CLLocation
        var locDate = loc.timestamp.timeIntervalSince1970
        var curDate = NSDate().timeIntervalSince1970
        var lat = loc.coordinate.latitude.description
        var lng = loc.coordinate.longitude.description
        
        // Reverse geocode and get the cityName
        if locationFlag {
            reverseGeoCode(loc)
            locationFlag = false
        }
    }
    
    func reverseGeoCode(location:CLLocation) {
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                println("Reverse geocoder failed with error" + error.localizedDescription)
                return
            }
            if placemarks.count > 0 {
                let pm = placemarks[0] as! CLPlacemark
                self.cityName = pm.locality + "," + " " + pm.administrativeArea
                var address = pm.locality + "," + pm.administrativeArea
                var cn = address.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
                var url = Constants.baseUrl + "?type=all&address=\(cn!)"

                self.fetchData(0, url: url)
            }
            else {
                println("Problem with the data received from geocoder")
            }
        })
    }
    
    func fetchData(modelIndex: Int, url: String) {
        let urlPath = NSURL(string: url)
        let request = NSURLRequest(URL: urlPath!)
        
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) { (response, data, error) -> Void in
                var jsonData = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as? NSDictionary
                if jsonData == nil {
                    jsonData = NSDictionary()
                }
                var model = DataFetch().processJson(jsonData!)
                if modelIndex == 0 {
                    model.name = self.cityName
                } else {
                    model.name = self.cities.cityModels[modelIndex].name
                    
                }
                self.cities.cityModels[modelIndex] = model
                self.buildCityViewFromModel(modelIndex)
        }
        
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Location Error \(error)")
    }
    
    func buildCityViewFromModel(index: Int) {
        pageControl.numberOfPages = cities.cityModels.count
        buildViewPoint = CGFloat(index) * self.view.bounds.width
        var currentView = buildCityViewFromNib(buildViewPoint, model: cities.cityModels[index])
        cityScrollView.addSubview(currentView)
        pageControl.currentPage = selectedIndex
    }
    
    func buildCityViewFromNib(startX:CGFloat, model: CityModel) -> InsideView {
        var cityView = InsideView.loadFromNibNamed("InsideView")
        
        cityView!.verticalScroll.contentSize = CGSizeMake(375, 2000)
        cityView!.frame = CGRectMake(startX, 0,self.view.bounds.width, self.view.bounds.height)
        cityView!.cityLabel.preferredMaxLayoutWidth = 200
        cityView!.cityLabel.text = model.name
        var pmIndex = 0
        var mainPMLabel = "NA"
        while mainPMLabel == "NA" && pmIndex < model.ozoneHourlyData.count {
            mainPMLabel = (model.pmHourlyData[pmIndex]["AQIValue"] as? String)!
            pmIndex++
        }
        cityView!.pmValue.text = mainPMLabel
        cityView!.pmValue.sizeToFit()
        cityView!.aqiCategory.text = "Air Quality - " + model.aqiCategory
        cityView!.aqiCategory.textColor = Utility.buildAQIBackgroundColor(model.aqiCategory)
        
        cityView!.aqiDescriptionLabel.numberOfLines = 0
        cityView!.aqiDescriptionLabel.text = Utility.buildAQIDescription(model.aqiCategory)
        cityView!.aqiDescriptionLabel.sizeToFit()
        
        makePMScrollView(cityView!.hourlyScrollView, x: 15, y: 45, pmData: model.pmHourlyData, timeFlag: false)
        makePMScrollView(cityView!.hourlyScrollView, x: 15, y: 10, pmData: model.pmHourlyData, timeFlag: true)
        //makePMScrollView(cityView!.hourlyScrollView, x: 15, y: 80, pmData: model.ozoneHourlyData, timeFlag: false)
        addTopBorderWithColor(UIColor.whiteColor(), width: 0.5, scrollView: cityView!.hourlyScrollView)
        
        addBottomBorderWithColor(UIColor.whiteColor(), width: 0.5, scrollView: cityView!.hourlyScrollView)
        
        cityView!.sevenDayTableView.delegate = self
        cityView!.sevenDayTableView.dataSource = self
        cityView!.sevenDayTableView.registerNib(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "sevenDayCell")
        cityView!.sevenDayTableView.reloadData()
        return cityView!
        
    }
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat, scrollView:UIView) {
        let border = CALayer()
        border.backgroundColor = color.CGColor
        border.frame = CGRectMake(0, 0, scrollView.frame.size.width, width)
        scrollView.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat, scrollView:UIView) {
        let border = CALayer()
        border.backgroundColor = color.CGColor
        border.frame = CGRectMake(0, scrollView.frame.height - width, scrollView.frame.size.width, width)
        scrollView.layer.addSublayer(border)
    }
    
    func makePMScrollView(scrollView: UIScrollView, var x:Int, y:Int, pmData: Array<NSDictionary>, timeFlag: Bool ) {
        for var i = 0;i < 7; ++i {
            var l:UILabel?
            if !timeFlag {
                l = makePMLabel(CGFloat(x), y: CGFloat(y), text: (pmData[i]["AQIValue"] as? String)!)
            } else {
                if i == 0 {
                   l = makePMLabel(CGFloat(x), y: CGFloat(y), text: "Now")
                } else {
                    l = makePMLabel(CGFloat(x), y: CGFloat(y), text: (pmData[i]["time"] as? String)!)
                }
                
            }
            scrollView.addSubview(l!)
            x = x + 60
        }
        scrollView.contentSize = CGSizeMake(CGFloat(x), scrollView.bounds.height)
    }
    
    func makePMLabel(x:CGFloat, y:CGFloat, text: String) -> UILabel {
        var label = UILabel(frame: CGRectMake(x, y, 50, 20))
        label.text = text
        label.textColor = UIColor.whiteColor()
        label.font = UIFont(name: label.font.fontName, size: 15)
        return label
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCellWithIdentifier("sevenDayCell") as! SevenDayTableViewCell
        var date = cities.cityModels[selectedIndex].pmDailyPeak[indexPath.row]["date"] as? String
        cell.dayLabel.text = Utility.getFullDayOfWeek(date!)
        var pmValue = (cities.cityModels[selectedIndex].pmDailyPeak[indexPath.row]["AQIValue"] as? String!)!.toInt()
        
        cell.pmLabel.text = cities.cityModels[selectedIndex].pmDailyPeak[indexPath.row]["AQIValue"] as? String
        if pmValue != nil {
           cell.pmIcon.image = Utility.buildAQIColor(pmValue!)
        }
        
        //cell.ozoneLabel.text = cities.cityModels[selectedIndex].ozoneDailyPeak[indexPath.row]["AQIValue"] as? String
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }

    @IBAction func onPageClick(sender: UIPageControl) {
        cityScrollView.scrollRectToVisible(CGRectMake(self.view.bounds.width * CGFloat(sender.currentPage), 0, self.view.bounds.width, self.view.bounds.height), animated: false)
    }
    
    override func viewDidAppear(animated: Bool) {
        if cities.cityModels.count > 0 {
            let contentWidth = cityScrollView.bounds.width * CGFloat(cities.cityModels.count)
            cityScrollView.contentSize = CGSizeMake(contentWidth, cityScrollView.bounds.height)
            
            var newScroll = self.view.bounds.width * CGFloat(selectedIndex)
            cityScrollView.scrollRectToVisible(CGRectMake(newScroll, 0, self.view.bounds.width, self.view.bounds.height), animated: false)
            
            
        }
    }
    
    @IBAction func addCity(sender: UIButton) {
        self.performSegueWithIdentifier("listOfCitiesSegue", sender: self)
        Flurry.logEvent("Add city clicked")
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        var width:CGFloat = scrollView.frame.size.width
        var page:Int = NSInteger((scrollView.contentOffset.x + (0.5 * width)) / width)
        pageControl.currentPage = page
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "listOfCitiesSegue" {
            var listOfCities = segue.destinationViewController as! ListOfCitiesViewController
            listOfCities.searchCityModels = cities
            
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func centralManagerDidUpdateState(central: CBCentralManager!) {
        if central.state == CBCentralManagerState.PoweredOn {
            NSTimer.scheduledTimerWithTimeInterval(100, target: self, selector: Selector("scanTimeout"), userInfo: nil, repeats: false)
            central.scanForPeripheralsWithServices([pmServiceUUID], options: nil)
        } else {
            println("Bluetooth is not powered on")
        }
    }
    
    @objc private func scanTimeout() {
        
        println("[DEBUG] Scanning stopped")
        self.centralManager.stopScan()
    }
    
    func centralManager(central: CBCentralManager!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [NSObject : AnyObject]!, RSSI: NSNumber!) {
        
        let nameOfDeviceFound = (advertisementData as NSDictionary).objectForKey(CBAdvertisementDataLocalNameKey) as? NSString
        self.peripheral = peripheral
        self.peripheral.delegate = self
        self.centralManager.connectPeripheral(peripheral, options: nil)
            }
    
    func centralManager(central: CBCentralManager!, didConnectPeripheral peripheral: CBPeripheral!) {
        peripheral.discoverServices(nil)
    }
    
    func peripheral(peripheral: CBPeripheral!, didDiscoverServices error: NSError!) {
        for service in peripheral.services {
            let thisService = service as! CBService
            peripheral.discoverCharacteristics(nil, forService: thisService)
            
            println(service.UUIDString)
        }
        
    }
    
    func peripheral(peripheral: CBPeripheral!, didDiscoverCharacteristicsForService service: CBService!, error: NSError!) {
        //cView.pmValue.text = "Enabling sensors"
        var enableByte = 1
        let enableBytes = NSData(bytes: &enableByte, length: sizeof(UInt8))
        for ch in service.characteristics {
            let thisCh = ch as! CBCharacteristic
            println(thisCh.UUID.description)
            if thisCh.UUID == pmServiceTxUUID {
                self.peripheral.setNotifyValue(true, forCharacteristic: thisCh)
                self.peripheral.readValueForCharacteristic(thisCh)
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral!, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!) {
        let dataBytes = characteristic.value
        /*
        let dataLength = dataBytes.length
        var dataArray = [Int8](count: dataLength, repeatedValue: 0)
        dataBytes.getBytes(&dataArray, length: dataLength * sizeof(Int8))
        println(dataArray)
        */
        var sensorValue: NSInteger = 0
        characteristic.value.getBytes(&sensorValue, length: 4)
        println(sensorValue)
        //cView.pmValue.text = String(sensorValue)
        
        // Element 1 of the array will be ambient temperature raw value
        // let ambientTemperature = Double(dataArray[1])/128
        
        // Display on the Status label
        //var dataString = NSString(data: dataBytes, encoding:NSUTF8StringEncoding)
        //self.statusLabel.text = String(dataString!)
    }
    
}
