//
//  SearchCityModels.swift
//  zenviron
//
//  Created by pbanavara on 06/06/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import Foundation

class SearchCityModels {
    var cityModels:Array<CityModel>!
    
    static let sharedInstance = SearchCityModels()
    
    init() {
        if cityModels == nil {
            var model = CityModel(name: "")
            cityModels = []
            cityModels.append(model)
        }
    }
}
