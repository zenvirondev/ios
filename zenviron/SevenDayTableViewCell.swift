//
//  SevenDayTableViewCell.swift
//  zenviron
//
//  Created by pbanavara on 03/08/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import UIKit

class SevenDayTableViewCell: UITableViewCell {

    @IBOutlet weak var ozoneLabel: UILabel!
    @IBOutlet weak var pmLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var pmIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
