//
//  Utility.swift
//  zenviron
//
//  Created by RAJEEV GOPALAKRISHNA on 7/2/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import Foundation
import UIKit

class Utility {
    static func getDayOfWeek(today:String)->String? {
        var d = [1:"Sun",2:"Mon",3:"Tue",4:"Wed",5:"Thu",6:"Fri",7:"Sat"]
        let formatter  = NSDateFormatter()
        formatter.dateFormat = "MM/dd/yy"
        if let todayDate = formatter.dateFromString(today) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            let myComponents = myCalendar.components(.CalendarUnitWeekday, fromDate: todayDate)
            let weekDay = myComponents.weekday
            return d[weekDay]
        } else {
            return "None"
        }
    }
    
    static func getFullDayOfWeek(today:String)->String? {
        var d = [1:"Sunday",2:"Monday",3:"Tuesday",4:"Wednesday",5:"Thursday",6:"Friday",7:"Saturday"]
        let formatter  = NSDateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        if let todayDate = formatter.dateFromString(today) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            let myComponents = myCalendar.components(.CalendarUnitWeekday, fromDate: todayDate)
            let weekDay = myComponents.weekday
            return d[weekDay]
        } else {
            return "None"
        }
    }
    
    static func buildAQIBackgroundColor(category:String) -> UIColor! {
        let greenColor = UIColor(red: 0/255.0, green: 228.0/255.0, blue:0.0, alpha: 1.0)
        let yellowColor = UIColor(red: 255/255.0, green: 255.0/255.0, blue:0.0, alpha: 1.0)
        let orangeColor = UIColor(red: 255/255.0, green: 126.0/255.0, blue:0.0, alpha: 1.0)
        
        let redColor = UIColor(red: 255/255.0, green: 0.0/255.0, blue:0.0, alpha: 1.0)
        let purpleColor = UIColor(red: 153/255.0, green: 0/255.0, blue:76.0/255.0, alpha: 1.0)
        let maroonColor = UIColor(red: 76/255.0, green: 0/255.0, blue:38.0/255.0, alpha: 1.0)
        
        let dictOfColors = ["Good": greenColor, "Moderate": yellowColor, "Unhealthy for Sensitive Groups": orangeColor, "Unhealthy": redColor, "Very Unhealthy": purpleColor, "Hazardous": maroonColor]
        return dictOfColors[category]
    }
    
    static func buildAQIColor(aqiValue:Int) -> UIImage! {
        var retColor: UIImage?
        let greenColor = UIColor(red: 0/255.0, green: 228.0/255.0, blue:0.0, alpha: 1.0)
        let yellowColor = UIColor(red: 255/255.0, green: 255.0/255.0, blue:0.0, alpha: 1.0)
        let orangeColor = UIColor(red: 255/255.0, green: 126.0/255.0, blue:0.0, alpha: 1.0)
        
        let redColor = UIColor(red: 255/255.0, green: 0.0/255.0, blue:0.0, alpha: 1.0)
        let purpleColor = UIColor(red: 153/255.0, green: 0/255.0, blue:76.0/255.0, alpha: 1.0)
        let maroonColor = UIColor(red: 76/255.0, green: 0/255.0, blue:38.0/255.0, alpha: 1.0)
        
        let dictOfColors = ["Good": UIImage(named: "dreaming_24_g.png"),
            "Moderate": UIImage(named: "dreaming_24_y.png"),
            "Unhealthy for Sensitive Groups": UIImage(named: "dreaming_24_o.png"),
            "Unhealthy": UIImage(named: "dreaming_24_r.png"),
            "Very Unhealthy": UIImage(named: "dreaming_24_p.png"),
            "Hazardous": UIImage(named: "dreaming_24_m.png")]
        
        if aqiValue <= 50 {
            retColor = dictOfColors["Good"]!
        } else if aqiValue > 50 && aqiValue <= 100 {
            retColor =  dictOfColors["Moderate"]!
        } else if aqiValue > 100 && aqiValue <= 150 {
            retColor = dictOfColors["Unhealthy for Sensitive Groups"]!
        } else if aqiValue > 150 && aqiValue <= 200 {
            retColor = dictOfColors["Unhealthy"]!
        } else if aqiValue > 200 && aqiValue <= 300 {
            retColor = dictOfColors["Very Unhealthy"]!
        } else if aqiValue > 300 && aqiValue <= 500 {
            retColor = dictOfColors["Hazardous"]!
        }
        return retColor
    }
    
    static func buildAQIDescription(category: String) -> String! {
        let goodDescription = "AQI is 0 - 50. Air quality is considered satisfactory, and air pollution poses little or no risk"
        let modDescription = "AQI is 51 - 100. Air quality is acceptable; however, some people who are unusually sensitive to ozone may experience respiratory symptoms"
        
        let unhealthySensitiveDescription = "AQI is 101 - 150. Although general public is not likely to be affected at this AQI range, people with lung disease, older adults and children are at a greater risk from exposure to ozone, whereas persons with heart and lung disease, older adults and children are at greater risk from the presence of particles in the air"
        
        let unhealthyDescription = "AQI is 151 - 200. Everyone may begin to experience some adverse health effects, and members of the sensitive groups may experience more serious effects"
        
        let vUnhealthyDescription = "AQI is 201 - 300. This would trigger a health alert signifying that everyone may experience more serious health effects"
        let hazardousDescription = "AQI greater than 300. This would trigger a health warnings of emergency conditions. The entire population is more likely to be affected."
        
        let descriptionDict = ["Good": goodDescription, "Moderate": modDescription, "Unhealthy for Sensitive Groups":unhealthySensitiveDescription, "Unhealthy": unhealthyDescription, "Very Unhealthy":vUnhealthyDescription, "Hazardous":hazardousDescription]
        return descriptionDict[category]
    }


    
    
    
    
}
