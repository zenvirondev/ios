//
//  DataFetch.swift
//  zenviron
//
//  Created by pbanavara on 22/06/15.
//  Copyright (c) 2015 pbanavara. All rights reserved.
//

import Foundation
import CoreLocation

class DataFetch {
    
    //Process JSON output from the backend
    func processJson(data:NSDictionary) -> CityModel {
        var cityModel = CityModel(name: "")
        if data.count > 0 {
            
            var pmData = data["PM2.5"] as! Array<NSDictionary>
            var ozoneData = data["OZONE"] as! Array<NSDictionary>
            var srcLatStr =  data["sourceLat"] as! NSString
            var srcLngStr = data["sourceLng"] as! NSString
            
            cityModel.localTime = data["localTime"] as! String
            cityModel.coordinates.latitude = srcLatStr.doubleValue
            cityModel.coordinates.longitude = srcLngStr.doubleValue
            
            var cLocation = CLLocation(latitude: cityModel.coordinates.latitude, longitude: cityModel.coordinates.longitude)
            cityModel.aqiCategory = data["AQICategory"] as! String
            
            for pm in pmData {
                var pmHourlyData: AnyObject?  = pm["hourly"]
                if pmHourlyData != nil {
                    cityModel.pmHourlyData = pmHourlyData as! Array<NSDictionary>
                }
                var pmDailyPeak: AnyObject? = pm["dailyPeak"]
                if pmDailyPeak != nil {
                    cityModel.pmDailyPeak = pmDailyPeak as! Array<NSDictionary>
                }
                var pmForecast: AnyObject? = pm["forecast"]
                if pmForecast != nil {
                    cityModel.pmForecastData = pmForecast as! Array<NSDictionary>
                }
            }
            
            for ozone in ozoneData {
                var ozHourlyData: AnyObject?  = ozone["hourly"]
                if ozHourlyData != nil {
                    cityModel.ozoneHourlyData = ozHourlyData as! Array<NSDictionary>
                }
                var ozDailyPeak: AnyObject? = ozone["dailyPeak"]
                if ozDailyPeak != nil {
                    cityModel.ozoneDailyPeak = ozDailyPeak as! Array<NSDictionary>
                }
                var ozForecast: AnyObject? = ozone["forecast"]
                if ozForecast != nil {
                    cityModel.ozoneForecastData = ozForecast as! Array<NSDictionary>
                }
                
            }
            
        }
        return cityModel
    }
    
}